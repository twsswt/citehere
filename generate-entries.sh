ENTRIES=`cat $1.ckeys`

##WORKING  DIR
ENTRIES_DIR="$1-entries"

##Executable variables

LATEX="latex"
BIBTEX="bibtex"

## Prevent any malformed .bbl or .tex files causing problems.
rm -rf $ENTRIES_DIR/

## Make a directory to store inline citation .bbl files
mkdir -p $ENTRIES_DIR
cp *.bib $ENTRIES_DIR/

for ENTRY in ${ENTRIES[@]}
do
echo "Preparing ["$ENTRY"] inline citation..."

##Removing trailing space from the entry (windows only)
LENGTH=${#ENTRY}
LENGTH=$LENGTH-1
ENTRY=${ENTRY:0:${LENGTH}}

##Prepare a .tex file from the template.  The template should contain
##at minimum a bibliography style command, a bibliography reference and a 
##\cite{BIBKEY}, where BIBKEY is a literal marker to be replaced by the 
##entry in the sed command below. 
sed -e "s/BIBKEY/${ENTRY}/g" incite-template.tex > $ENTRIES_DIR/$ENTRY.tex

##Generate the .bbl file.
cd $ENTRIES_DIR
"${LATEX}" $ENTRY.tex;
cd ..

cd $ENTRIES_DIR
"${BIBTEX}" $ENTRY;
cd ..

## Remove the thebibliography environment and the bibitem macro
## from the bbl file.
sed -i -e 's/\\begin{thebibliography}{}//g' $ENTRIES_DIR/$ENTRY.bbl
sed -i -e 's/\\begin{thebibliography}{[0-9]*}//g' $ENTRIES_DIR/$ENTRY.bbl
sed -i -e 's/\\end{thebibliography}//g' $ENTRIES_DIR/$ENTRY.bbl


BIBITEM="\\bibitem{$ENTRY}"

sed  -i -e  "s/\\\\bibitem{$ENTRY}//g" $ENTRIES_DIR/$ENTRY.bbl

sed -i -e '/\\bibitem/ {N ; s/\\bibitem\[[[:alnum:][:space:][:graph:]\n]*\]{[[:alnum:][:punct:]]*}//}' $ENTRIES_DIR/$ENTRY.bbl

#sed -i -e '/^$/d' entries/$ENTRY.bbl

done
