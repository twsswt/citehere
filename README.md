citehere package setup and usage instructions.

The citehere package enables the inclusion of bibtex references any
where in a latex document, rather than within a thebibliography
environment.

Setup

1. Ensure that the generate-entries.sh file is on your PATH and is
executable.

2. Prepare a template.tex file in the same directory as your main
latex source document.  A sample template is included in the
distribution.  The template is a simple latex document that, if used
with normal latex, bibtex would generate a template.aux and
template.bbl file, although latex would warn of missing citations, and
bibtex would warn that it couldn't find the key 'BIBKEY'.  The
template includes a single cite command for a BIBKEY.  This will be
used by the citehere package and should not be changed.  You should
replace the bibliography style plain with your preferred bibliography
style (and add any required supporting packages) and replace the
tws/bibliography with the path to your .bib file.

3. Make sure that latex is aware of citehere package, either by
re-building the file database, or including the citehere.sty file in
the same directory as your latex document.

Usage

1. Prepare a latex document, placing citehere commands with
appropriate keys where the reference details should be included.

2. Run latex on the source document.  This will generate a file called
inlinecitations.keys in the same directory as your source document.
This is a newline separated list of the keys used in the citedhere
command.  Reviewing the compiled document (e.g. in yapp) will show the
citehere commands displayed as 'PLACEHOLDER FOR <citation key>'.

3. Run generate-entries.sh in the same directory as your source
document.  This will create a directory called entries/ containing
instances of template.tex for each citehere key, as well as modified
.bbl files that will be included in the main document.

4. Run latex again.  This time, the placeholders should be replaced by
the information contained in the individual entries/<citation key>.bbl
files.

Limitations

1. Currently, latex will fail if more than one citation key is
included in the citehere command.  This is because the thebibliography
environment delimiters will not be removed correclty by sed.

2. Inserting reference details directly into the document source means
that cross referencing is disabled foor citehere entries.  That is,
the bibitem macro normally associated with each reference in a
thebibliography list is removed and the contents of the .bbl are
treated like normal text.  Normal cite and nocite commands are
unaffected.
